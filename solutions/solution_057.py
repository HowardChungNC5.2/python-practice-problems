# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(n):           # solution
    sum = 0                             # solution
    # starting from 1 up to
    for i in range(1, n + 1):           # solution
        sum += i / (i + 1)              # solution
    return sum                          # solution

input_one = 1
input_two = 2
input_three = 3

print(sum_fraction_sequence(input_one))
print(sum_fraction_sequence(input_two))
print(sum_fraction_sequence(input_three))
