# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def find_second_largest(values):
#     if len(values) <= 1:
#         return None
#     # set max val to val at first index of given list of ints
#     max_value = values[0]
#     # set 2nd largest val to undefined (None)
#     second_largest = None
#     # loop over list of vals starting from index 1
#     for value in values[1:]:
#         # if current value is greater than max value
#         if value > max_value:
#             # max value replaces 2nd largest
#             second_largest = max_value
#             # current value becomes new max value
#             max_value = value
#             print("max:", max_value)
#             print("current num:", value)
#             print("2nd largest:", second_largest)
#         # otherwise, if 2nd largest val is not defined OR current val is greater than 2nd largest val
#         elif second_largest is None or value > second_largest:
#             # set 2nd largest val to current value
#             second_largest = value
#             print("max:", max_value)
#             print("current num:", value)
#             print("2nd largest:", second_largest)
#     # return 2nd largest val
#     return second_largest

def find_second_largest(values):
    # input is a list of ints
    # return 2nd largest int in values

    if len(values) <= 1:
        return None

    maximum = values[0]
    second_largest = None

    for num in values[1:]:
        if num > maximum:
            second_largest = maximum
            maximum = num
        elif second_largest is None or num > second_largest:
            second_largest = num
    return second_largest

three_expected = [4,3,2,1]
four_expected = [2,3,4,5]

print(find_second_largest(three_expected))
print(find_second_largest(four_expected))
