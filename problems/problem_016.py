# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    return (0 <= x <= 10) and (0 <= y <= 10)

print(is_inside_bounds(0, 10)) # True
print(is_inside_bounds(2, 9)) # True
print(is_inside_bounds(0, 13)) # False
print(is_inside_bounds(-3, 7)) # False
