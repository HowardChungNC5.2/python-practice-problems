# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    new_dictionary = {}                                 # solution
    # loop over each KV pair item
    for key, value in dictionary.items():               # solution
        # instantiate new item in container dict
        new_dictionary[value] = key                     # solution
    return new_dictionary                               # solution
    # pass                                              # problem

# def reverse_dictionary(dictionary):
#     # return new dict with given dictionary.value = key and dictionary.keys = values
#     new_dict = {}
#     for key, value in dictionary.items():
#         # new_dict = {
#         #     dictionary.key: key
#         # }
#         # current_value = dictionary.key
#         # new_dict.current_value = key
#         print("key:", key)
#         print("value:", value)
#         new_dict[value] = key
#     return new_dict

result1 = {}
result2 = {"key", "value"}
result3 = {"one": 1, "two": 2, "three": 3}

print(reverse_dictionary(result1))
print(reverse_dictionary(result2))
print(reverse_dictionary(result3))
