# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def sum_of_first_n_even_numbers(n):
#     if n < 0:                                           # solution
#         return None                                     # solution
#     # create accumulator
#     sum = 0                                             # solution
#     # loop as many times as given count inclusive
#     for i in range(n + 1):                              # solution
#         # during each loop increment sum by next even num
#         sum = sum + i * 2                               # solution
#     # return accumulator
#     return sum                                          # solution

def sum_of_first_n_even_numbers(n):
    # input is a numerical count
    # return sum of the even nums starting from 0 up to n
    if n < 0:
        return None

    sum = 0
    for i in range(n + 1):
        # even nums = [0,2,4,6,8,...]
        sum = sum + (i*2)
    return sum

expected_none = -1
expected_zero = 0
expected_two = 1
expected_six = 2
expected_thirty = 5

print(sum_of_first_n_even_numbers(expected_none))
print(sum_of_first_n_even_numbers(expected_zero))
print(sum_of_first_n_even_numbers(expected_two))
print(sum_of_first_n_even_numbers(expected_six))
print(sum_of_first_n_even_numbers(expected_thirty))
