# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7

def sum_two_numbers(x, y):
    # return sum of given nums
    return x + y

num_one = 3
num_two =  4
print(sum_two_numbers(num_one, num_two))
