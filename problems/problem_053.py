# Write a function that meets these requirements.
#
# Name:       username_from_email
# Parameters: a valid email address as a string
# Returns:    the username portion of the email address
#
# The username portion of an email is the substring
# of the email address that appears before the @
#
# Examples
#    * input:   "basia@yahoo.com"
#      returns: "basia"
#    * input:   "basia.farid@yahoo.com"
#      returns: "basia.farid"
#    * input:   "basia_farid+test@yahoo.com"
#      returns: "basia_farid+test"

def username_from_email(email_address):
    # split string to a list with "@" separator
    # return val before @
    # split = email_address.split("@")
    # print(split)
    # return split[0]
    return email_address.split("@")[0]

input_one = "basia@yahoo.com"
input_two = "basia.farid@yahoo.com"
input_three = "basia_farid+test@yahoo.com"
input_four = "howard_chung@yahoo.com"

print(username_from_email(input_one))
print(username_from_email(input_two))
print(username_from_email(input_three))
print(username_from_email(input_four))
