# Modify the withdraw method of BankAccount so that the bank
# account can not have a negative balance.
#
# If a person tries to withdraw more than what is in the
# balance, then the method should raise a ValueError.

class BankAccount:
    def __init__(self, balance):
        self.balance = balance

    def get_balance(self):
        return self.balance

    def withdraw(self, amount):
        # If the amount is more than what is in
        if amount > self.balance:
            # the balance, then raise a ValueError
            raise ValueError
        self.balance -= amount

    def deposit(self, amount):
        self.balance += amount

account_one = BankAccount(10000)
account_one.withdraw(500000)
print(account_one.get_balance())
