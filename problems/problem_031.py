# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_squares(values):
    if len(values) == 0:
        return None
    # input is list of ints
    # return sum of each num squared pow(num, 2)
    sum_squared = 0
    for num in values:
        sum_squared += pow(num, 2)
    return sum_squared

expected_none = []
expected_fourteen = [1,2,3]
expected_two = [-1,0,1]

print(sum_of_squares(expected_none))
print(sum_of_squares(expected_fourteen))
print(sum_of_squares(expected_two))
