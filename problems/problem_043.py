# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.

# def find_indexes(search_list, search_term):
#     results = []                                        # solution
#     # create an enumerate object, key:counter and value:search_list
#     # access the index and value of the enumerate object
#     for index, value in enumerate(search_list):         # solution
#         if value == search_term:                        # solution
#             results.append(index)                       # solution
#     return results                                      # solution
#     # pass                                              # problem

# SOLUTION FUNCTION SYNTAX
def find_indexes(search_list, search_term):
    index_list = []
    # create an enumerate object, key:counter and value:search_list
    # enumerate_object = enumerate(search_list)
    # print(list(enumerate_object))
    # access the index and value of current item in enumerate obj
    for index, value in enumerate(search_list):
        # if value is search_term
        # print("index:", index)
        # print("value:", value)
        if value == search_term:
            # add index to result container
            index_list.append(index)
    return index_list

# HOWARD'S FUNCTION SYNTAX
def find_indexes(search_list, search_term):
    # return new_list containing entries for the index of the search_term in search_list
    index_list = []
    index = 0
    # loop over search_list
    for item in search_list:
        # if current item is search_term
        if item == search_term:
            # grab index location
            # push index location to index_list
            # print("item:", item)
            index_list.append(index)
            index += 1
        # otherwise
        else:
            # increment index by 1
            index += 1
    return index_list

search_list_one = [1,2,3,4,5]
search_term_one = 4
search_list_two = [1,2,3,4,5]
search_term_two = 6
search_list_three = [1,2,1,2,1]
search_term_three = 1

print(find_indexes(search_list_one, search_term_one)) # [3]
print(find_indexes(search_list_two, search_term_two)) # [6]
print(find_indexes(search_list_three, search_term_three)) # [0, 2, 4]
