# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    # create empty list
    list = []
    # if workday: True and sunny: False
    if is_workday and not is_sunny:
        # add "umbrella" to list
        list.append("umbrella")
    # otherwise, if workday: True
    elif is_workday:
        # add "laptop" to list
        list.append("laptop")
    # otherwise
    elif not is_workday:
        # add "surfboard" to list
        list.append("surfboard")
    # return list
    return list

print(gear_for_day(is_workday=True, is_sunny=False))
print(gear_for_day(is_workday=True, is_sunny=True))
print(gear_for_day(is_workday=False, is_sunny=False))
