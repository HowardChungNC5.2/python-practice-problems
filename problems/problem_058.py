# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.

def group_cities_by_state(city_state_list):
    # return a dict = {
        # "2-LETTER STATE": [city_one, city_two, ..]
    # }

    state_with_cities = {}
    # access each entry in the given list
    for entry in city_state_list:
        # split current entry to city and state on "," separator"
        city, state = entry.split(",")
        # if current state does NOT exists in result collection
        if state not in state_with_cities:
            # instantiate the property set to an empty list
            state_with_cities[state] = []
        # otherwise, add city to associated state
        state_with_cities[state].append(city)
    return state_with_cities

texas_cities = ['San Antonio, TX']
massachusetts_cities = ['Springfield, MA', 'Boston, MA']
ohio_cities = ['Cleveland, OH', 'Columbus, OH', 'Chicago, IL']

print(group_cities_by_state(texas_cities))
print(group_cities_by_state(massachusetts_cities))
print(group_cities_by_state(ohio_cities))
