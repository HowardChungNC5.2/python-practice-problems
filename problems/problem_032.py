# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
# def sum_of_first_n_numbers(limit):
#     if limit < 0:                                       # solution
#         return None                                     # solution
#     # create an accumulator
#     sum = 0                                             # solution
#     # increment accumulator from 0 up to limit inclusive
#     for i in range(limit + 1):                          # solution
#         # increment sum by num at current index
#         sum = sum + i                                   # solution
#     return sum                                          # solution
#     # pass

def sum_of_first_n_numbers(limit):
    if limit < 0:
        return None
    # input is numerical limit
    # return sum of nums from 0 up to limit inclusive
    sum = 0
    for num in range(limit + 1):
        sum += num
    return sum

expected_none = -1
expected_zero = 0
expected_one = 1
expected_three = 2
expected_fifteen = 5

print(sum_of_first_n_numbers(expected_none))
print(sum_of_first_n_numbers(expected_zero))
print(sum_of_first_n_numbers(expected_one))
print(sum_of_first_n_numbers(expected_three))
print(sum_of_first_n_numbers(expected_fifteen))
