# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

def pairwise_add(list1, list2, list3):
    results = []                                        # solution
    # create a zip obj (iterator of tuples), where corresponding items of given iterables (ex. lists)
    for value1, value2, value3 in zip(list1, list2, list3):            # solution
        # sum = value1 + value2 + value3
        # print("sum:", sum)
        results.append(value1 + value2 + value3)                 # solution
    return results                                      # solution
    # pass                                              # problem

result1a = [1,2,3,4]
result1b = [4,5,6,7]
result1c = [2,4,6,8]
#expected: [7,11,15,19]
result2a = [100,200,300]
result2b = [10,1,180]
result2c = [1,2,3]
#expected: [111,203,483]

print(pairwise_add(result1a, result1b, result1c))
print(pairwise_add(result2a, result2b, result2c))

# def pairwise_add(list1, list2):
#     # given two lists of same length
#     # return new list with sum of corresponding entries in two lists
#     # new_list = [
#         # "list1[0]+list2[0]",
#         # "list1[1]+list2[1]",
#         # "list1[2]+list2[2]"
#         # ]
#     new_list = []
#     # loop over list1
#     sum = 0
#     for value1 in list1:
#         # loop over list2
#         sum += value1
#         for value2 in list2:
#             # add curr list1 num + curr list2 num
#             sum += value2
#             print('sum:', sum)
#         # push sum to new list
#         new_list.append(sum)
#     return new_list

# result1a = [1,2,3,4]
# result1b = [4,5,6,7]
# result2a = [100,200,300]
# result2b = [10,1,180]

# print(pairwise_add(result1a, result1b))
# print(pairwise_add(result2a, result2b))
