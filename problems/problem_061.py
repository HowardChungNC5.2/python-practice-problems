# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]

def remove_duplicates(values_list):
    # return copy of list removing all duplicate values and keep original order
    # loop over list
        # if current val not in list
            # add to val to container
    # return container
    original_vals = []
    for val in values_list:
        if val not in original_vals:
            original_vals.append(val)
    return original_vals

print(remove_duplicates([1,1,1,1]))
print(remove_duplicates([1,2,2,1]))
print(remove_duplicates([1,3,3,20,3,2,2]))
