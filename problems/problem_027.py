# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None

    return max(list(values))

ten_max = [1,2,10]
nine_max = [3,9,6]

print(max_in_list(ten_max))
print(max_in_list(nine_max))
