# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!

import math

def safe_divide(numerator, denominator):
    # if denominator is zero
        # return math.inf.
    # else
        # return num / denom
    if denominator == 0:
        return math.inf
    else:
        return numerator / denominator

numerator_zero = 1
denominator_zero = 0
numerator_result = 4
denominator_result = 2

print(safe_divide(numerator_zero, denominator_zero))
print(safe_divide(numerator_result, denominator_result))
