# Write a function that meets these requirements.
#
# Name:       temperature_differences
# Parameters: highs: a list of daily high temperatures
#             lows: a list of daily low temperatures
# Returns:    a new list containing the difference
#             between each high and low temperature
#
# The two lists will be the same length
#
# Example:
#     * inputs:  highs: [80, 81, 75, 80]
#                lows:  [72, 78, 70, 70]
#       result:         [ 8,  3,  5, 10]

# def temperature_differences(highs, lows):       # solution
#     differences = []                            # solution
#     for high, low in zip(highs, lows):          # solution
#         differences.append(high - low)          # solution
#     return differences                          # solution

def temperature_differences(highs, lows):
    # return a new_list containing difference between each high and low pair
    temps = []
    # zip() both lists to create an iterator of tuples
    # ((80, 72), (81, 78), (75, 70), (80, 70))
    # print(tuple(zip(highs, lows)))
    # loop over zipped list and grab each high entry and low entry
    for high, low in zip(highs, lows):
        # push difference between high and low to temps container
        temps.append(high - low)
    return temps

highs_list = [80, 81, 75, 80]
lows_list = [72, 78, 70, 70]
print(temperature_differences(highs_list, lows_list))
