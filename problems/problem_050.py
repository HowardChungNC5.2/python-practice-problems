# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(input):
    # return two new lists
    first_list = []
    second_list = []
    # first_list length is floor of given list len / 2 + remainder of given list len / 2
    first_list_len = len(input) // 2 + (len(input) % 2)
    # loop over given list up to list length non-inclusive
    for i in range(first_list_len):
        first_list.append(input[i])
    # start 2nd loop from halfway point, considering odd length of list
    for i in range(len(input) // 2):
        # grab val after end of first list
        index = + first_list_len
        second_list.append(input[index])
    return first_list, second_list

list_even = [1, 2, 3, 4]
list_odd = [1, 2, 3, 4, 5]

print(halve_the_list(list_even))
print(halve_the_list(list_odd))
