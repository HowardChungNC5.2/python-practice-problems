# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one

import random

def specific_random():
    nums = []
    # access all nums btwn btwn 10 and 500, inclusive by looping
    for i in range(10, 500+1):
        # if current num is divisble by 35 (5and7) without a remainder
        if i % 35 == 0:
            # add num to nums collection
            nums.append(i)
        # user random.choice to select one
    return random.choice(nums)

print(specific_random())
