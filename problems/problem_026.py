# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    # input is a list of integers
    # 0 < number < 100
    # return avg of values

    avg = 0
    sum = 0
    total_nums = len(values)

    for num in values:
        sum += num
    avg = sum / total_nums
    print(sum, avg, total_nums)

    if avg < 60:
        return "F"
    elif 60 <= avg < 70:
        return "D"
    elif 70 <= avg < 80:
        return "C"
    elif 80 <= avg < 90:
        return "B"
    else:
        return "A"

valuesA = [90, 90]
valuesB = [80, 80]
valuesC = [70, 70]
valuesD = [60, 60]
valuesF = [50, 50]
print(calculate_grade(valuesA))
print(calculate_grade(valuesB))
print(calculate_grade(valuesC))
print(calculate_grade(valuesD))
print(calculate_grade(valuesF))
