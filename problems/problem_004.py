# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    # return max value of given args
    # if two values are same max value
        # return either value
    # if all vals are same
        # return any val
    #  use >=
    maximum = value1
    if value2 > maximum and value2 > value3:
        maximum = value2
        return maximum
    elif value3 > maximum and value3 > value2:
        maximum = value3
        return maximum
    return maximum

print(max_of_three(1, 2, 3))
print(max_of_three(2, 3, 1))
print(max_of_three(3, 2, 1))
