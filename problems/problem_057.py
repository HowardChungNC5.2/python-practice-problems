# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(num):
    # return sum of fractions form num/num+1
    # 1/2+2/3+3/4+...+number/number+1
    sum = 0
    # loop starting from 1 up to given num, inclusive
    for i in range(1, num + 1):
        # increment sum by current num / num + 1
        sum += i / (i + 1)
    return sum

input_one = 1
input_two = 2
input_three = 3
input_four = 4

print(sum_fraction_sequence(input_one))
print(sum_fraction_sequence(input_two))
print(sum_fraction_sequence(input_three))
print(sum_fraction_sequence(input_four))
