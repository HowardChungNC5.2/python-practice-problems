# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

# GLEARN FUNCTION SYNTAX
def translate(key_list, dictionary):
    result = []                                         # solution
    for key in key_list:                                # solution
        # no matter what, push the value or None at dictionary.key to new_list
        result.append(dictionary.get(key))              # solution
    return result                                       # solution
    # pass                                              # problem

# HOWARD'S FUNCTION SYNTAX
# def translate(key_list, dictionary):
#     # return new list containing values of corresponding keys in given dictionary, i.e. if key list entry matches dict key push the corresponding dict value to new_list
#     new_list = []
#     # grab key in key_list
#     for key in key_list:
#         # USE .GET() METHOD TO ACCESS DICT VALUE AT KEY
#         print("KEY:", key)
#         print("dictionary value:", dictionary.get(key))
#         # if value exists add to new_list
#         dict_value = dictionary.get(key)
#         if dict_value:
#             new_list.append(dict_value)
#         else:
#             new_list.append(None)
#     return new_list

keys1 = ["name", "age"]
dict1 = {"name": "Noor", "age": 29}
keys2 = ["eye color", "age"]
dict2 = {"name": "Noor", "age": 29}
keys3 = ["age", "age", "age"]
dict3 = {"name": "Noor", "age": 29}

print(translate(keys1, dict1)) # expected ["Noor", 29]
print(translate(keys2, dict2)) # expected [None, 29]
print(translate(keys3, dict3)) # expected [29, 29, 29]
