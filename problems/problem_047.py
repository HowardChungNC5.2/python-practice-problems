# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    # check password contains:
    # set Boolean flag to False for all class password properties
    has_alpha_lower_case = False
    has_alpha_upper_case = False
    has_digit = False
    has_special_char = False
    # access each char in password
    for char in password:
        # .isalpha()
        if char.isalpha():
            # .islower()
            # * It must have at least one lowercase letter (a-z)
            if char.islower():
                has_alpha_lower_case = True
            # .isupper()
            # * It must have at least one uppercase letter (A-Z)
            elif char.isupper():
                has_alpha_upper_case = True
        # .isdigit()
        elif char.isdigit():
            # * It must have at least one digit (0-9)
            has_digit = True
        # * It must have at least one special character $, !, or @
        elif char in ["$", "!", "@"]:
            has_special_char = True
    # 6 <= len(password) <= 12
    # * It must have six or more characters in it
    # * It must have twelve or fewer characters in it
    return (6 <= len(password) <= 12
    and has_alpha_lower_case
    and has_alpha_upper_case
    and has_digit
    and has_special_char
    )

password_pass = "12abCD!?"
print(check_password(password_pass)) #expected True
password_fail_lower = "12ABCD!?"
print(check_password(password_fail_lower)) #expected False
password_fail_lower = "12ABCD!?"
print(check_password(password_fail_lower)) #expected False
password_fail_upper = "12abcde!?"
print(check_password(password_fail_upper)) #expected False
password_fail_special_char = "12abcde##"
print(check_password(password_fail_special_char)) #expected False
password_fail_length = "12aC?"
print(check_password(password_fail_length)) #expected False
