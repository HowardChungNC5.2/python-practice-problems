# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    # input is a string
    # return a str with all duplicates removed
    # split_chars = s.split("")
    # print(split_chars)
    removed_string = ""
    for char in s:
        if char not in removed_string:
            removed_string += char
    return removed_string

helo_expected = "heellllooo"
print(remove_duplicate_letters(helo_expected))
