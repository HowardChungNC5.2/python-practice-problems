# Write a function that meets these requirements.
#
# Name:       generate_lottery_numbers
# Parameters: none
# Returns:    a list of six random unique numbers
#             between 1 and 40, inclusive
#
# Example bad results:
#    [4, 2, 3, 3, 1, 5] duplicate numbers
#    [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
# https://docs.python.org/3/library/random.html

import random

def generate_lottery_numbers():
    # return a list of six random unique nums btwn 1 and 40, inclusive
    numbers =[]
    # keep looping until container length is 5
    while len(numbers) < 6:
        # grab a randon num btwn 1 and 40, inclusive
        num = random.randint(1, 40)
        # if num is not in container
        if num not in numbers:
            # add num to container
            numbers.append(num)
    return numbers

print(generate_lottery_numbers())
