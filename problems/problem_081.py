# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"

class Animal:
    def __init__(self, number_of_legs, primary_color):
        self.number_of_legs = number_of_legs
        self.primary_color = primary_color

    def describe(self):
        return f"{self.__class__.__name__} has {str(self.number_of_legs)} legs and is primarily {self.primary_color}"

# animal_panda = Animal(4, "black and white")
# print(animal_panda.describe())

class Dog(Animal):
    def speak(self):
        return "Bark!"

dog = Dog(4, "golden")
print(dog.number_of_legs)
print(dog.primary_color)
print(dog.describe())
print(dog.speak())

class Cat(Animal):
    def speak(self):
        return "Miao!"

cat = Cat(4, "tabby")
print(cat.number_of_legs)
print(cat.primary_color)
print(cat.describe())
print(cat.speak())

class Snake(Animal):
    def speak(self):
        return "Ssssssss!"

snake = Snake(0, "green")
print(snake.number_of_legs)
print(snake.primary_color)
print(snake.describe())
print(snake.speak())
