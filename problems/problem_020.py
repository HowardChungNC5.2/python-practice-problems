# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    return len(attendees_list) >= len(members_list) * 0.5

print(has_quorum([1,2,3], [1,2,3,4])) # True
print(has_quorum([1,2,], [1,2,3,4])) # True
print(has_quorum([1,], [1,2,3,4])) # False
