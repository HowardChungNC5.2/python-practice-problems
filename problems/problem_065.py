# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

def biggest_gap(nums_list):
    # return largest gap btwn any two consecutive nums
    # set largest gap to diff btwn first two ints
    largest_gap = abs(nums_list[1] - nums_list[0])
    # get diff btwn next two ints starting from index 1
    for i in range(1, len(nums_list) - 1):
        diff = abs(nums_list[i + 1] - nums_list[i])
        if diff > largest_gap:
            largest_gap = diff
    return largest_gap

nums_one = [1, 3, 5, 7]
nums_two = [1, 11, 9, 20, 0]
nums_three = [1, 3, 100, 103, 106]

print(biggest_gap(nums_one))
print(biggest_gap(nums_two))
print(biggest_gap(nums_three))
