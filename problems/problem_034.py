# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2

def count_letters_and_digits(s):
    # input is a string
    # return two vals,
        # number of chars in given str
            # use .isalpha() to test if char
        # number of digits in given str
            # use .isdigit() method to test if digit

    # loop over str
        str_count = 0
        digit_count = 0
        for char in s:
            # if current char is str
            if char.isalpha():
                # increment str accumulator
                str_count += 1
            # if current char is digit
            if char.isdigit():
                # increment digit accumulator
                digit_count += 1
        return str_count, digit_count

expected_zero_zero = ""
expected_one_zero = "a"
expected_zero_one = "1"
expected_one_one = "1a"

print(count_letters_and_digits(expected_zero_zero))
print(count_letters_and_digits(expected_one_zero))
print(count_letters_and_digits(expected_zero_one))
print(count_letters_and_digits(expected_one_one))
