# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    # input is a comma separated list
    # ["8,1,7", "10,10,10", "1,2,3"]
    summed_list = []
    # loop over given list
    for item in csv_lines:
        # split current entry on comma separator
        entry = item.split(",")
        # create accumulator to store total sum of each entry
        total = 0
        # loop over each value in current entry
        for value in entry:
            # convert val to int
            current_num = int(value)
            # increment accumulator with current int
            total += current_num
        # add sum of curr entry to new list
        summed_list.append(total)
    # return new list with each entry being the corresponding sum of nums in each entry
    return summed_list

result1 = []
result2 = ["3", "1,9"]
result3 = ["8,1,7", "10,10,10", "1,2,3"]

print(add_csv_lines(result1))
print(add_csv_lines(result2))
print(add_csv_lines(result3))
