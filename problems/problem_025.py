# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    # if vals is empty
        # return None
    # return sum of given list of vals
    if len(values) == 0:
        return None

    sum = 0
    for num in values:
        sum += num
    return sum

print(calculate_sum([1,2,3,4])) # expected 10
print(calculate_sum([-1,2,3,-4])) # expected 0
print(calculate_sum([])) # expected None
