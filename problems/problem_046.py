# Complete the make_sentences function that accepts three
# lists.
#   * subjects contains a list of subjects for three-word sentences
#   * verbs contains a list of verbs for three-word sentences
#   * objects contains a list of objects for three-word sentences
# The make_sentences function should return all possible sentences
# that can be made from the words in each list
#
# Examples:
#   * subjects: ["I"]
#     verbs:    ["play"]
#     objects:  ["Portal"]
#     returns:  ["I play Portal"]
#   * subjects: ["I", "You"]
#     verbs:    ["play"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "You play Portal", "You play Sable"]
#   * subjects: ["I", "You"]
#     verbs:    ["play", "watch"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "I watch Portal", "I watch Sable",
#                "You play Portal", "You play Sable"
#                "You watch Portal", "You watch Sable"]
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

def make_sentences(subjects, verbs, objects):
    # return all possible three-word sentences with words from each given list
    combined_sentence = []

    for subject in subjects:
        # print("SUBJECT:", subject)
        for verb in verbs:
            # print("verb:", verb)
            for object in objects:
                # print("object:", object)
                sentence = f"{subject} {verb} {object}"
                # push sentence to collection container
                combined_sentence.append(sentence)
    return combined_sentence

subjects_1 = ["I"]
verbs_1 = ["play"]
objects_1 = ["Portal"]
subjects_2 = ["I", "You"]
verbs_2 = ["play"]
objects_2 = ["Portal", "Sable"]
subjects_3 = ["I", "You"]
verbs_3 = ["play", "watch"]
objects_3 = ["Portal", "Sable"]

print(make_sentences(subjects_1, verbs_1, objects_1))
print(make_sentences(subjects_2, verbs_2, objects_2))
print(make_sentences(subjects_3, verbs_3, objects_3))
